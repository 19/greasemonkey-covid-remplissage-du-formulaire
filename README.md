# Greasemonkey covid remplissage du formulaire

Remplissage automatique du formulaire de sortie du gouvernement.
https://media.interieur.gouv.fr/deplacement-covid-19/


## Installation
1.  [Installer greaseMonkey](https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/)
2.  Cliquer sur l'icone de GreaseMonkey
3.  Cliquer sur "New user script"
3.  Copier le code du fichier **Formulaire covid - courses.txt** dans la fenête du script GreaseMonkey
4.  Sauvegarder (Ctrl + S)
5.  Aller sur la page du formulaire susmentionné
6.  Cliquer sur le nom du script souhaité
7.  Cliquer sur "**User script option**"
8.  Cliquer sur le + a sous le champ "**User includes**"
9.  PROFIT

Idéalement, dupliquer le script dans GreaseMonkey avec un case a cocher pour chaque motif de sortie et changer le nom du script pour avoir : 
*  Formulaire covid - courses
*  Formulaire covid - travail
*  ...

Le nom du script est a changé dans l'en-tête du script, le champ @name